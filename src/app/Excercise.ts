// export interface Todo {
//   userId: number;
//   id: number;
//   title: string;
//   body: string;
// }

export interface Excercise {
  id: number;
  uuid: string;
  exercise_base: number;
  image: string;
  is_main: boolean;
  status: string;
  style: string;
}
