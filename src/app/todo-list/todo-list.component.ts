import { Observable, map, shareReplay } from 'rxjs';
import { ExcerciseListServiceService } from '../excercise-list-service.service';
import { Component, OnInit } from '@angular/core';
import { Excercise } from '../Excercise';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent implements OnInit {
  excercises$: Observable<Excercise[]> = this.excerciseService.getExcercise();
  takeValues: any;

  constructor(private excerciseService: ExcerciseListServiceService) {}

  excercise = {
    id: 76,
    uuid: '2fc71cc3-6c30-43f8-a6eb-61c74d129030',
    exercise_base: 641,
    image: 'https://wger.de/media/exercise-images/76/T-bar-row-1.png',
    is_main: true,
    status: '2',
    style: '1',
  };

  ngOnInit(): void {
    this.excerciseService.excerciseList$.subscribe(
      (res) => (this.takeValues = res)
    );
  }

  save() {
    this.excerciseService.createTodo(this.excercise).subscribe();
  }
}
