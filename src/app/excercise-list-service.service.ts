import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Excercise } from './Excercise';

@Injectable({
  providedIn: 'root',
})
export class ExcerciseListServiceService implements OnInit {
  constructor(private httpClient: HttpClient) {}

  excercise = new BehaviorSubject<Excercise[]>([]);
  excerciseList$ = this.excercise.asObservable();

  ngOnInit(): void {
    console.log(this.excerciseList$);
  }

  getExcercise(): Observable<Excercise[]> {
    var header = {
      headers: new HttpHeaders().set(
        'Authorization',
        'Token 46800b8343b85a2558a50dc5f5e661c274ecd340'
      ),
    };
    this.httpClient
      .get<Excercise[]>('https://wger.de/api/v2/exerciseimage/', header)
      .subscribe((response: any) => {
        this.excercise.next(response['results']);
      });
    return this.excerciseList$;
  }

  createTodo(createTodo: Excercise): Observable<Excercise> {
    return this.httpClient.post<Excercise>(
      `${environment.apiUrl}/users/1/todos`,
      createTodo
    );
  }

  deleteTodo(id: number): Observable<{}> {
    return this.httpClient.delete<Excercise>(
      `${environment.apiUrl}/users/1/todos/${id}`
    );
  }

  updateTodo(todo: Excercise): Observable<Excercise> {
    return this.httpClient.put<Excercise>(
      `${environment.apiUrl}/users/1/todos`,
      todo
    );
  }
}
